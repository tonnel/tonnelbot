#include "tnnl_db.hpp"

#include <iostream>
#include <ostream>


namespace Tnnl {


DB::DB(const std::string& db_file):
  db_file_(db_file) {

  int error = sqlite3_open(db_file_.c_str(), &conn_);

  if (error) {
    std::cerr << "can't open database: " << sqlite3_errmsg(conn_) << std::endl;
    std::exit(1);

  } else
    std::cout << "opened database successfully" << std::endl;

  create_tables();
}

DB::~DB() {
  sqlite3_close(conn_);
}

int DB::dummy_cb(void*, int argc, char** argv, char** col_name) {
  return SQLITE_OK;
}

void DB::create_tables() {
  int error;
  char* err_msg = 0;

  std::vector<std::string> create_strings {
    "CREATE TABLE IF NOT EXISTS proxy(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," \
    "chat_id INT NOT NULL, message_id INT NOT NULL," \
    "proxy_url TEXT NOT NULL, fail_count INT NOT NULL DEFAULT 0," \
    "last_check TEXT, country_code TEXT, country_name TEXT, message TEXT);",
    "CREATE UNIQUE INDEX IF NOT EXISTS proxy_id_idx ON proxy(id ASC);",
    "CREATE UNIQUE INDEX IF NOT EXISTS chat_msg_idx ON proxy(chat_id ASC, message_id ASC);",

    "CREATE TABLE IF NOT EXISTS user(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, username TEXT, key TEXT UNIQUE, super_user INTEGER DEFAULT 0);",
    "CREATE UNIQUE INDEX IF NOT EXISTS user_id_idx ON user(id ASC);",
    "CREATE UNIQUE INDEX IF NOT EXISTS user_key_idx ON user(key ASC);",

    "CREATE TABLE IF NOT EXISTS session(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, user_id INTEGER NOT NULL, chat_id INTEGER NOT NULL);",
    "CREATE UNIQUE INDEX IF NOT EXISTS session_id_idx ON session(id ASC);",
    "CREATE UNIQUE INDEX IF NOT EXISTS session_user_chat_id_idx ON session(user_id ASC, chat_id ASC);"
  };

  for (auto const& sql : create_strings) {
    error = sqlite3_exec(conn_, sql.c_str(), &DB::dummy_cb, 0, &err_msg);

    if (error != SQLITE_OK) {
      std::cerr << "cant create tables. SQL error: " << err_msg << std::endl;
      sqlite3_free(err_msg);
      std::exit(1);
    }
  };

}

Proxy::Ptr DB::add_proxy(TgBot::Message::Ptr ch_post, const std::string& proxy_url) {
  std::lock_guard<std::mutex> locker(mutex_);

  int error;
  char* err_msg = 0;

  std::ostringstream sql;

  sql << "INSERT INTO proxy (chat_id,message_id,proxy_url,message) "
      << "VALUES ("
      << ch_post->chat->id << ","
      << ch_post->messageId << ","
      << "'" << proxy_url << "',"
      << "'" << ch_post->text << "');";

  error = sqlite3_exec(conn_, sql.str().c_str(), &DB::dummy_cb, 0, &err_msg);

  if (error != SQLITE_OK) {
    std::cerr << "Cant add proxy. SQL error: " << err_msg << std::endl;
    sqlite3_free(err_msg);
    return nullptr;

  } else {
    Proxy::Ptr proxy = std::make_shared<Proxy>();

    proxy->set_id(sqlite3_last_insert_rowid(conn_));
    proxy->set_chat_id(ch_post->chat->id);
    proxy->set_message_id(ch_post->messageId);
    proxy->set_url(proxy_url);
    proxy->set_message(ch_post->text);

    std::cout << "added proxy id: " << proxy->get_id() << ", " << proxy->get_url() << std::endl;
    return proxy;
  }
}

void DB::get_users(User::Vec& users, TgBot::Message::Ptr message) {
  std::lock_guard<std::mutex> locker(mutex_);

  int error;
  char* err_msg = 0;

  std::string key = message->text;

  auto callback = [](void* data, int argc, char** argv, char** col_name) {
    User::Vec* users = static_cast<User::Vec*>(data);
    User::Ptr user = std::make_shared<User>();

    std::istringstream id_ss(argv[0]);
    std::istringstream key_ss(argv[2]);
    std::istringstream super_user_ss(argv[3]);

    int32_t id;
    std::string key;
    bool super_user;

    id_ss >> id;
    key_ss >> key;
    super_user_ss >> super_user;

    user->set_id(id);
    user->set_key(key);
    user->set_super_user(super_user);

    users->push_back(user);

    return SQLITE_OK;
  };

  std::ostringstream sql;

  sql << "SELECT * FROM user WHERE key='" << key << "'";

  error = sqlite3_exec(conn_, sql.str().c_str(), callback, static_cast<void*>(&users), &err_msg);

  if (error != SQLITE_OK) {
    std::cerr << "Cant get user. SQL error: " << err_msg << std::endl;
    sqlite3_free(err_msg);
  }

}

void DB::get_proxies(Proxy::Vec& proxy_vec) {

  std::lock_guard<std::mutex> locker(mutex_);

  int error;
  char* err_msg = 0;

  auto callback = [](void* data, int argc, char** argv, char** col_name) {
    Proxy::Vec* proxy_vec = static_cast<Proxy::Vec*>(data);
    Proxy::Ptr proxy_ptr = std::make_shared<Proxy>();

    std::istringstream id_ss(argv[0]);
    std::istringstream chat_id_ss(argv[1]);
    std::istringstream message_id_ss(argv[2]);
    std::istringstream url_ss(argv[3]);
    std::istringstream fail_count_ss(argv[4]);
    std::istringstream last_check_ss(argv[5] ? argv[5] : "");

    int32_t id;
    int64_t chat_id;
    int64_t message_id;
    std::string url;
    int16_t fail_count;
    std::string last_check;
    std::string country_code;
    std::string country_name;
    std::string message;

    id_ss >> id;
    chat_id_ss >> chat_id;
    message_id_ss >> message_id;
    url_ss >> url;
    fail_count_ss >> fail_count;
    last_check_ss >> last_check;

    if (argv[6])
      country_code = std::string(argv[6]);

    if (argv[7])
      country_name = std::string(argv[7]);

    if (argv[8])
      message = std::string(argv[8]);

    proxy_ptr->set_id(id);
    proxy_ptr->set_chat_id(chat_id);
    proxy_ptr->set_message_id(message_id);
    proxy_ptr->set_url(url);
    proxy_ptr->set_fail_count(fail_count);
    proxy_ptr->set_last_check(last_check);
    proxy_ptr->set_country_code(country_code);
    proxy_ptr->set_country_name(country_name);
    proxy_ptr->set_message(message);

    proxy_vec->push_back(proxy_ptr);

    return SQLITE_OK;
  };

  std::ostringstream sql;

  sql << "SELECT * FROM proxy;";

  error = sqlite3_exec(conn_, sql.str().c_str(), callback, static_cast<void*>(&proxy_vec), &err_msg);

  if (error != SQLITE_OK) {
    std::cerr << "Cant get proxies. SQL error: " << err_msg << std::endl;
    sqlite3_free(err_msg);
  }
}

void DB::get_sessions(Session::Vec& sessions) {
  std::lock_guard<std::mutex> locker(mutex_);

  int error;
  char* err_msg = 0;

  auto callback = [](void* data, int argc, char** argv, char** col_name) {
    Session::Vec* sessions = static_cast<Session::Vec*>(data);
    Session::Ptr session = std::make_shared<Session>();

    std::istringstream id_ss(argv[0]);
    std::istringstream user_id_ss(argv[1]);
    std::istringstream chat_id_ss(argv[2]);

    int32_t id;
    int32_t user_id;
    int64_t chat_id;

    id_ss >> id;
    user_id_ss >> user_id;
    chat_id_ss >> chat_id;

    session->set_id(id);
    session->set_user_id(user_id);
    session->set_chat_id(chat_id);

    sessions->push_back(session);

    return SQLITE_OK;
  };

  std::ostringstream sql;

  sql << "SELECT * FROM session;";

  error = sqlite3_exec(conn_, sql.str().c_str(), callback, static_cast<void*>(&sessions), &err_msg);

  if (error != SQLITE_OK) {
    std::cerr << "Cant get sessions. SQL error: " << err_msg << std::endl;
    sqlite3_free(err_msg);
  }
}

void DB::update_proxy(Proxy::Ptr proxy) {
  std::lock_guard<std::mutex> locker(mutex_);

  int error;
  char* err_msg = 0;

  std::ostringstream sql;

  sql << "UPDATE proxy SET "
      << "fail_count=" << proxy->get_fail_count() << ","
      << "last_check='" << proxy->get_last_check() << "',"
      << "country_code='" << proxy->get_country_code() << "',"
      << "country_name='" << proxy->get_country_name() << "' "
      << "WHERE id=" << proxy->get_id() << ";";

  error = sqlite3_exec(conn_, sql.str().c_str(), &DB::dummy_cb, 0, &err_msg);

  if (error != SQLITE_OK) {
    std::cerr << "Cant udpate proxy id: " << proxy->get_id() << ". SQL error: " << err_msg << std::endl;
    sqlite3_free(err_msg);
  }
}

void DB::delete_proxy(Proxy::Ptr proxy) {
  std::lock_guard<std::mutex> locker(mutex_);

  int error;
  char* err_msg = 0;

  std::ostringstream sql;

  sql << "DELETE FROM proxy "
      << "WHERE id=" << proxy->get_id() << ";";

  error = sqlite3_exec(conn_, sql.str().c_str(), &DB::dummy_cb, 0, &err_msg);

  if (error != SQLITE_OK) {
    std::cerr << "Cant delete proxy id: " << proxy->get_id() << ". SQL error: " << err_msg << std::endl;
    sqlite3_free(err_msg);
  }
}

void DB::add_session(TgBot::Message::Ptr message, User::Ptr user) {
  std::lock_guard<std::mutex> locker(mutex_);

  int error;
  char* err_msg = 0;

  std::ostringstream sql;

  sql << "INSERT INTO session (chat_id,user_id) "
      << "VALUES ("
      << message->chat->id << ","
      << user->get_id() << ");";

  error = sqlite3_exec(conn_, sql.str().c_str(), &DB::dummy_cb, 0, &err_msg);

  if (error != SQLITE_OK) {
    std::cerr << "Cant add session. SQL error: " << err_msg << std::endl;
    sqlite3_free(err_msg);

  } else
    std::cout << "added user session: " << user->get_username() << std::endl;

}

} // namespace Tnnl
