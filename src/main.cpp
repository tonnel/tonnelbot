#include <iostream>
#include <thread>
#include <vector>

#include "boost/program_options.hpp"

#include "tnnl_bot.hpp"

namespace b_po = boost::program_options;

int main(int argc, char** argv) {
  try {
    b_po::options_description opts;

    opts.add_options()
    ("db,d", b_po::value<std::string>()->default_value("tonnel.db"), "Path to database.")
    ("key,k", b_po::value<std::string>(), "Bot API key.")
    ("interval,i", b_po::value<int>()->default_value(5), "Proxy check interval.")
    ("fail,f", b_po::value<int>()->default_value(5), "Number of failed checks before proxy removal.")
    ("threads,t", b_po::value<int>()->default_value(2), "Number of pinger threads.")
    ("help,h", "Print this help message and exit.");

    b_po::variables_map options;

    b_po::store(b_po::parse_command_line(argc, argv, opts), options);

    if (options.count("help") || !options.count("key")) {
      std::cout << "Usage:\n\n" << opts << std::endl;
      return EXIT_SUCCESS;
    }

    Tnnl::Bot tonnel_bot(options);

    std::vector<std::thread> service_threads;

    for (int i = 0; i < options.at("threads").as<int>(); i++)
      service_threads.push_back(std::move(std::thread(&Tnnl::Bot::start_service, &tonnel_bot)));

    tonnel_bot.start_polling();

    for (auto& thread : service_threads)
      thread.join();

    return EXIT_SUCCESS;

  } catch (std::exception& e) {
    std::cerr << "main error: " << e.what() << std::endl;

    return EXIT_FAILURE;
  }
}
