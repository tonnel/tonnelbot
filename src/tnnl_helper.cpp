#include "tnnl_helper.hpp"

#include <vector>
#include <regex>

#include "tgbot/tools/StringTools.h"


namespace Tnnl {

// *INDENT-OFF*
const std::map<std::string, std::string> Emoji
{
  {"Chart With Upwards Trend", "📈"},
  {"Chart With Downwards Trend", "📉"},
  {"Bar Chart", "📊"},
  {"Pushpin", "📌"},
  {"Red Exclamation Mark", "❗"},
  {"Three O’clock", "🕒"},
  {"Paperclip", "📎"},
  {"Round Pushpin", "📍"},
  {"Hourglass With Flowing Sand", "⏳"},
  {"Hourglass", "⌛"},
  {"Watch", "⌚"},
  {"Alarm Clock", "⏰"},
  {"Stopwatch", "⏱️"},
  {"Timer Clock", "⏲️"},
  {"World Map", "🗺️"},
  {"Copyright Sign", "©️"},
  {"Registered Sign", "®️"},
  {"Cross Mark", "❌"},
  {"Multiplication Symbol", "✖️"},
  {"Checkmark", "✔️"},
  {"Check Mark", "✅"},
  {"Ballot Box With Check", "☑️"},
  {"Negative Squared Cross Mark", "❎"},
  {"Red Question Mark", "❓"},
  {"White Question Mark", "❔"},
  {"Red Exclamation Mark and Question Mark", "⁉️"},
  {"Red Exclamation Mark", "❗"},
  {"White Exclamation Mark", "❕"},
  {"Red Double Exclamation Mark", "‼️"},
  {"Plus Symbol", "➕"},
  {"Minus Symbol", "➖"},
  {"Up Arrow", "⬆️"},
  {"Up-Right Arrow", "↗️"},
  {"Right Arrow", "➡️"},
  {"Down-Right Arrow", "↘️"},
  {"Down Arrow", "⬇️"},
  {"Down-Left Arrow", "↙️"},
  {"Left Arrow", "⬅️"},
  {"Up-Left Arrow", "↖️"},
  {"Up-Down Arrow", "↕️"},
  {"Left-Right Arrow", "↔️"}
};

const std::map<std::string, std::string> Flags
{
  {"AC", "🇦🇨"}, {"AD", "🇦🇩"}, {"AE", "🇦🇪"}, {"AF", "🇦🇫"}, {"AG", "🇦🇬"}, {"AI", "🇦🇮"}, {"AL", "🇦🇱"}, {"AM", "🇦🇲"},
  {"AO", "🇦🇴"}, {"AQ", "🇦🇶"}, {"AR", "🇦🇷"}, {"AS", "🇦🇸"}, {"AT", "🇦🇹"}, {"AU", "🇦🇺"}, {"AW", "🇦🇼"}, {"AX", "🇦🇽"},
  {"AZ", "🇦🇿"}, {"BA", "🇧🇦"}, {"BB", "🇧🇧"}, {"BD", "🇧🇩"}, {"BE", "🇧🇪"}, {"BF", "🇧🇫"}, {"BG", "🇧🇬"}, {"BH", "🇧🇭"},
  {"BI", "🇧🇮"}, {"BJ", "🇧🇯"}, {"BL", "🇧🇱"}, {"BM", "🇧🇲"}, {"BN", "🇧🇳"}, {"BO", "🇧🇴"}, {"BQ", "🇧🇶"}, {"BR", "🇧🇷"},
  {"BS", "🇧🇸"}, {"BT", "🇧🇹"}, {"BV", "🇧🇻"}, {"BW", "🇧🇼"}, {"BY", "🇧🇾"}, {"BZ", "🇧🇿"}, {"CA", "🇨🇦"}, {"CC", "🇨🇨"},
  {"CD", "🇨🇩"}, {"CF", "🇨🇫"}, {"CG", "🇨🇬"}, {"CH", "🇨🇭"}, {"CI", "🇨🇮"}, {"CK", "🇨🇰"}, {"CL", "🇨🇱"}, {"CM", "🇨🇲"},
  {"CN", "🇨🇳"}, {"CO", "🇨🇴"}, {"CP", "🇨🇵"}, {"CR", "🇨🇷"}, {"CU", "🇨🇺"}, {"CV", "🇨🇻"}, {"CW", "🇨🇼"}, {"CX", "🇨🇽"},
  {"CY", "🇨🇾"}, {"CZ", "🇨🇿"}, {"DE", "🇩🇪"}, {"DG", "🇩🇬"}, {"DJ", "🇩🇯"}, {"DK", "🇩🇰"}, {"DM", "🇩🇲"}, {"DO", "🇩🇴"},
  {"DZ", "🇩🇿"}, {"EA", "🇪🇦"}, {"EC", "🇪🇨"}, {"EE", "🇪🇪"}, {"EG", "🇪🇬"}, {"EH", "🇪🇭"}, {"ER", "🇪🇷"}, {"ES", "🇪🇸"},
  {"ET", "🇪🇹"}, {"EU", "🇪🇺"}, {"FI", "🇫🇮"}, {"FJ", "🇫🇯"}, {"FK", "🇫🇰"}, {"FM", "🇫🇲"}, {"FO", "🇫🇴"}, {"FR", "🇫🇷"},
  {"GA", "🇬🇦"}, {"GB", "🇬🇧"}, {"GD", "🇬🇩"}, {"GE", "🇬🇪"}, {"GF", "🇬🇫"}, {"GG", "🇬🇬"}, {"GH", "🇬🇭"}, {"GI", "🇬🇮"},
  {"GL", "🇬🇱"}, {"GM", "🇬🇲"}, {"GN", "🇬🇳"}, {"GP", "🇬🇵"}, {"GQ", "🇬🇶"}, {"GR", "🇬🇷"}, {"GS", "🇬🇸"}, {"GT", "🇬🇹"},
  {"GU", "🇬🇺"}, {"GW", "🇬🇼"}, {"GY", "🇬🇾"}, {"HK", "🇭🇰"}, {"HM", "🇭🇲"}, {"HN", "🇭🇳"}, {"HR", "🇭🇷"}, {"HT", "🇭🇹"},
  {"HU", "🇭🇺"}, {"IC", "🇮🇨"}, {"ID", "🇮🇩"}, {"IE", "🇮🇪"}, {"IL", "🇮🇱"}, {"IM", "🇮🇲"}, {"IN", "🇮🇳"}, {"IO", "🇮🇴"},
  {"IQ", "🇮🇶"}, {"IR", "🇮🇷"}, {"IS", "🇮🇸"}, {"IT", "🇮🇹"}, {"JE", "🇯🇪"}, {"JM", "🇯🇲"}, {"JO", "🇯🇴"}, {"JP", "🇯🇵"},
  {"KE", "🇰🇪"}, {"KG", "🇰🇬"}, {"KH", "🇰🇭"}, {"KI", "🇰🇮"}, {"KM", "🇰🇲"}, {"KN", "🇰🇳"}, {"KP", "🇰🇵"}, {"KR", "🇰🇷"},
  {"KW", "🇰🇼"}, {"KY", "🇰🇾"}, {"KZ", "🇰🇿"}, {"LA", "🇱🇦"}, {"LB", "🇱🇧"}, {"LC", "🇱🇨"}, {"LI", "🇱🇮"}, {"LK", "🇱🇰"},
  {"LR", "🇱🇷"}, {"LS", "🇱🇸"}, {"LT", "🇱🇹"}, {"LU", "🇱🇺"}, {"LV", "🇱🇻"}, {"LY", "🇱🇾"}, {"MA", "🇲🇦"}, {"MC", "🇲🇨"},
  {"MD", "🇲🇩"}, {"ME", "🇲🇪"}, {"MF", "🇲🇫"}, {"MG", "🇲🇬"}, {"MH", "🇲🇭"}, {"MK", "🇲🇰"}, {"ML", "🇲🇱"}, {"MM", "🇲🇲"},
  {"MN", "🇲🇳"}, {"MO", "🇲🇴"}, {"MP", "🇲🇵"}, {"MQ", "🇲🇶"}, {"MR", "🇲🇷"}, {"MS", "🇲🇸"}, {"MT", "🇲🇹"}, {"MU", "🇲🇺"},
  {"MV", "🇲🇻"}, {"MW", "🇲🇼"}, {"MX", "🇲🇽"}, {"MY", "🇲🇾"}, {"MZ", "🇲🇿"}, {"NA", "🇳🇦"}, {"NC", "🇳🇨"}, {"NE", "🇳🇪"},
  {"NF", "🇳🇫"}, {"NG", "🇳🇬"}, {"NI", "🇳🇮"}, {"NL", "🇳🇱"}, {"NO", "🇳🇴"}, {"NP", "🇳🇵"}, {"NR", "🇳🇷"}, {"NU", "🇳🇺"},
  {"NZ", "🇳🇿"}, {"OM", "🇴🇲"}, {"PA", "🇵🇦"}, {"PE", "🇵🇪"}, {"PF", "🇵🇫"}, {"PG", "🇵🇬"}, {"PH", "🇵🇭"}, {"PK", "🇵🇰"},
  {"PL", "🇵🇱"}, {"PM", "🇵🇲"}, {"PN", "🇵🇳"}, {"PR", "🇵🇷"}, {"PS", "🇵🇸"}, {"PT", "🇵🇹"}, {"PW", "🇵🇼"}, {"PY", "🇵🇾"},
  {"QA", "🇶🇦"}, {"RE", "🇷🇪"}, {"RO", "🇷🇴"}, {"RS", "🇷🇸"}, {"RU", "🇷🇺"}, {"RW", "🇷🇼"}, {"SA", "🇸🇦"}, {"SB", "🇸🇧"},
  {"SC", "🇸🇨"}, {"SD", "🇸🇩"}, {"SE", "🇸🇪"}, {"SG", "🇸🇬"}, {"SH", "🇸🇭"}, {"SI", "🇸🇮"}, {"SJ", "🇸🇯"}, {"SK", "🇸🇰"},
  {"SL", "🇸🇱"}, {"SM", "🇸🇲"}, {"SN", "🇸🇳"}, {"SO", "🇸🇴"}, {"SR", "🇸🇷"}, {"SS", "🇸🇸"}, {"ST", "🇸🇹"}, {"SV", "🇸🇻"},
  {"SX", "🇸🇽"}, {"SY", "🇸🇾"}, {"SZ", "🇸🇿"}, {"TA", "🇹🇦"}, {"TC", "🇹🇨"}, {"TD", "🇹🇩"}, {"TF", "🇹🇫"}, {"TG", "🇹🇬"},
  {"TH", "🇹🇭"}, {"TJ", "🇹🇯"}, {"TK", "🇹🇰"}, {"TL", "🇹🇱"}, {"TM", "🇹🇲"}, {"TN", "🇹🇳"}, {"TO", "🇹🇴"}, {"TR", "🇹🇷"},
  {"TT", "🇹🇹"}, {"TV", "🇹🇻"}, {"TW", "🇹🇼"}, {"TZ", "🇹🇿"}, {"UA", "🇺🇦"}, {"UG", "🇺🇬"}, {"UM", "🇺🇲"}, {"UN", "🇺🇳"},
  {"US", "🇺🇸"}, {"UY", "🇺🇾"}, {"UZ", "🇺🇿"}, {"VA", "🇻🇦"}, {"VC", "🇻🇨"}, {"VE", "🇻🇪"}, {"VG", "🇻🇬"}, {"VI", "🇻🇮"},
  {"VN", "🇻🇳"}, {"VU", "🇻🇺"}, {"WF", "🇼🇫"}, {"WS", "🇼🇸"}, {"XK", "🇽🇰"}, {"YE", "🇾🇪"}, {"YT", "🇾🇹"}, {"ZA", "🇿🇦"},
  {"ZM", "🇿🇲"}, {"ZW", "🇿🇼"}, {"gbeng", "🏴󠁧󠁢󠁥󠁮󠁧󠁿"}, {"gbsct", "🏴󠁧󠁢󠁳󠁣󠁴󠁿"}, {"gbwls", "🏴󠁧󠁢󠁷󠁬󠁳󠁿"}, {"Chequered Flag", "🏁"}, {"Triangular Flag on Post", "🚩"},
  {"Crossed Flags", "🎌"}, {"Black Flag", "🏴"}, {"White Flag", "🏳️"}, {"Rainbow Flag", "🏳️‍🌈"}, {"Jolly Roger", "🏴‍☠️"},
  {"Globe Showing Europe and Africa", "🌍"}, {"Globe Showing Americas", "🌎"}, {"Globe Showing Asia-Australia", "🌏"},
  {"Globe With Meridians", "🌐"}, {"World Map", "🗺️"}
};


namespace Tools {

void extract_url_from_post(const std::string& post, std::string& url) {
  std::regex valid_url("https:\\/\\/t\\.me\\/proxy\\?(server=.*)&(port=\\d{0,5})&secret=(\\w{32})");
  std::smatch match;

  if(std::regex_search(post, match, valid_url)) {
      url = match[0];
  }
}


void extract_host_from_url(const std::string& url, std::string& host) {
  std::vector<std::string> url_struct = StringTools::split(url, '/');
  std::vector<std::string> path_and_params = StringTools::split(url_struct[3], '?');
  std::vector<std::string> params = StringTools::split(path_and_params[1], '&');
  std::vector<std::string> host_param = StringTools::split(params[0], '=');

  host = host_param[1];
}

bool check_proxy_url(const std::string& message) {
  std::regex valid_url("^https:\\/\\/t\\.me\\/proxy\\?(server=.*)&(port=\\d{0,5})&secret=(\\w{32})$");
  std::cmatch match;

  if (std::regex_match(message.c_str(), match, valid_url))
      return true;
  else
      return false;
}
// *INDENT-ON*

} // namespace Tools
} // namespace Tnnl
