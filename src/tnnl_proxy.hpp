#ifndef TNNL_PROXY_HPP
#define TNNL_PROXY_HPP

#include <cstdint>
#include <string>
#include <iostream>
#include <memory>
#include <vector>

namespace Tnnl {

class Proxy {
 public:
  using Ptr = std::shared_ptr<Proxy>;
  using Vec = std::vector<Proxy::Ptr>;

  Proxy();
  ~Proxy() {}

  void set_id(int32_t id) {id_ = id;}
  void set_chat_id(int64_t chat_id) {chat_id_ = chat_id;}
  void set_message_id(int64_t message_id) {message_id_ = message_id;}
  void set_url(const std::string& url) {url_ = url;}
  void set_fail_count(int16_t fail_count) {fail_count_ = fail_count;}
  void set_last_check(const std::string& last_check) {last_check_ = last_check;}
  void set_country_code(const std::string& country_code) {country_code_ = country_code;}
  void set_country_name(const std::string& country_name) {country_name_ = country_name;}
  void set_message(const std::string& message) {message_ = message;}

  int32_t get_id() const { return id_;}
  int64_t get_chat_id() const { return chat_id_;}
  int64_t get_message_id() const { return message_id_;}
  const std::string& get_url() { return url_;}
  int16_t get_fail_count() const { return fail_count_;}
  const std::string& get_last_check() { return last_check_;}
  const std::string& get_country_code() { return country_code_;}
  const std::string& get_country_name() { return country_name_;}
  const std::string& get_message() { return message_;}

  int16_t enc_fail_count() { return ++fail_count_;}
  int16_t dec_fail_count() { return --fail_count_;}

  void set_last_check();
  void set_country();

 private:
  int32_t id_ = -1;
  int64_t chat_id_;
  int64_t message_id_;
  std::string url_;
  int16_t fail_count_ = 0;
  std::string last_check_;
  std::string country_code_;
  std::string country_name_;
  std::string message_;
};

} // namespace Tnnl

#endif // TNNL_PROXY_HPP
