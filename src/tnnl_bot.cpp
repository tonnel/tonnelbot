#include "tnnl_bot.hpp"

#include <iostream>

#include <functional>
#include <chrono>

#include "date.h"

namespace Tnnl {

Bot::Bot(b_po::variables_map& opts):
  io_(),
  signals_(io_, SIGINT, SIGTERM),
  checker_(io_),
  opts_(opts),
  db_(opts.at("db").as<std::string>()),
  tg_bot_(opts.at("key").as<std::string>()) {

  setup_signals();
  setup_handlers();
  start_checker();
}

void Bot::setup_signals() {
  signals_.async_wait([&](const boost::system::error_code & error, int signal_number) {

    if (error)
      return;

    std::cout << "shutdown by signal " << signal_number << std::endl;
    io_.stop();
  });
}

void Bot::setup_handlers() {
  std::cout << "bot username: " << tg_bot_.getApi().getMe()->username << std::endl;

  tg_bot_.getEvents().onCommand("start", [&](TgBot::Message::Ptr message) {
    tg_bot_.getApi().sendMessage(message->chat->id, "Send me your API key now.");
  });

  tg_bot_.getEvents().onAnyMessage([&](TgBot::Message::Ptr message) {
    if (StringTools::startsWith(message->text, "/start"))
      return;

    if (message->text.size() != 32) {
      tg_bot_.getApi().sendMessage(message->chat->id, "Private access only. Contact @fakedude for API key.");
      return;
    }

    try_auth(message);
  });

  tg_bot_.getEvents().onInlineQuery([&](TgBot::InlineQuery::Ptr query) {
    if (Tools::check_proxy_url(query->query)) {
      std::cerr << "inline query NOT IMPLEMENTED. from. User: " << query->from->username << ", query: " << query->query << std::endl;

      /*
      std::string proxy_host;

      Tools::extract_host_from_url(query->query, proxy_host);

      if (proxy_host.empty())
        return;

      TgBot::InlineKeyboardMarkup::Ptr keyboard(new TgBot::InlineKeyboardMarkup);
      std::vector<TgBot::InlineKeyboardButton::Ptr> row0;
      TgBot::InlineKeyboardButton::Ptr checkButton(new TgBot::InlineKeyboardButton);
      checkButton->text = "check";
      checkButton->callbackData = "check";
      row0.push_back(checkButton);
      keyboard->inlineKeyboard.push_back(row0);

      TgBot::InlineQueryResultArticle::Ptr res(new TgBot::InlineQueryResultArticle);
      TgBot::InputTextMessageContent::Ptr msg(new TgBot::InputTextMessageContent);

      msg->messageText = "BOdy TEXT MUST BE HERE";
      msg->type = "article";

      res->id = query->id;
      res->title = "proxy_host";
      res->caption = "CAPTION MMMMS";
      res->type = "article";
      res->inputMessageContent = msg;
      res->replyMarkup = keyboard;

      std::vector<TgBot::InlineQueryResult::Ptr> answer;

      answer.push_back(res);

      tg_bot_.getApi().answerInlineQuery(query->id, answer, 300, true);
      */
    }
  });
}

void Bot::start_service() {
  io_.run();
}

void Bot::try_add_proxy(ChannelPost::Ptr ch_post) {
  Pinger::Ptr pinger;

  std::string host;
  std::string err;

  Tools::extract_host_from_url(ch_post->proxy_url_, host);

  if (host.empty()) {
    std::cerr << "no host in url: " << ch_post->proxy_url_ << std::endl;
    return;
  }

  if (pinger->ping(host, 2, err)) {
    Proxy::Ptr proxy = db_.add_proxy(ch_post->channel_message_, ch_post->proxy_url_);
    proxy->set_last_check();
    proxy->set_country();

    if (proxy)
      update_message(proxy);

  } else {
    std::cerr << "dead proxy: " << ch_post->proxy_url_ << std::endl;

    io_.post([&, ch_post]() {
      delete_message(ch_post);
    });
  }
}

void Bot::process_update(TgBot::Update::Ptr update) {
  ChannelPost::Ptr ch_post(new ChannelPost);

  ch_post->channel_message_ = update->channelPost;

  Tools::extract_url_from_post(ch_post->text(), ch_post->proxy_url_);

  if (ch_post->has_url())
    io_.post(std::bind(&Bot::try_add_proxy, this, ch_post));

  else
    std::cerr << "no proxy in string: " << ch_post->text() << std::endl;
}

void Bot::process_report(Report::Ptr report) {
  try {
    std::ostringstream report_stream;

    if (report->failed_.size() == report->total_) {
      std::cerr << "all proxies are failed!" << std::endl;
      report_stream << "all proxies are failed! please inspect!" << std::endl;

    } else {
      report->admin_ = "admin";
      report_stream << *report;

      if (report->failed_.size() > 0) {
        report_stream << "\nFailed:\n";

        for (auto const& proxy : report->failed_) {
          io_.post(std::bind(&Bot::update_message, this, proxy, Score::Down));
          report_stream << proxy->get_url() << "\n";
          std::cerr << "[" << std::this_thread::get_id() << "] failed proxy: " << proxy->get_url() << std::endl;
        }
      }

      if (report->removed_.size() > 0) {
        report_stream << "\nRemoved:\n";

        for (auto const& proxy : report->removed_) {
          db_.delete_proxy(proxy);

          io_.post([&, proxy]() {
            delete_message(proxy);
          });

          report_stream << proxy->get_url() << "\n";
          std::cerr << "[" << std::this_thread::get_id() << "] removed proxy: " << proxy->get_url() << std::endl;
        }
      }
    }

    Session::Vec sessions;
    db_.get_sessions(sessions);

    for (auto const& session : sessions)
      tg_bot_.getApi().sendMessage(session->get_chat_id(), report_stream.str());

  } catch (std::exception& e) {
    std::cerr << "report admins error: " << e.what() << std::endl;
  }
}

void Bot::delete_message(ChannelPost::Ptr ch_post) {
  try {
    std::lock_guard<std::mutex> locker(update_mutex_);

    tg_bot_.getApi().deleteMessage(ch_post->channel_message_->chat->id, ch_post->channel_message_->messageId);

  } catch (std::exception& e) {
    std::cerr << "delete message error: " << e.what() << std::endl;
  }
}


void Bot::delete_message(Proxy::Ptr proxy) {
  try {
    std::lock_guard<std::mutex> locker(update_mutex_);

    tg_bot_.getApi().deleteMessage(proxy->get_chat_id(), proxy->get_message_id());

  } catch (std::exception& e) {
    std::cerr << "delete message error: " << e.what() << std::endl;
  }
}

void Bot::start_polling() {
  std::thread polling([&]() {
    try {
      std::cout << "polling started" << std::endl;
      TgBot::TgLongPoll longPoll(tg_bot_);

      while (true) {
        std::vector<TgBot::Update::Ptr> updates = tg_bot_.getApi().getUpdates();

        for (const auto& update : updates) {
          if (update->channelPost != nullptr)
            process_update(update);
        }

        longPoll.start();
      }

    } catch (std::exception& e) {
      std::cerr << "polling error: " << e.what() << std::endl;
      io_.post(std::bind(&Bot::start_polling, this));
    }
  });

  polling.detach();
}

void Bot::start_checker() {
  checker_.expires_from_now(boost::posix_time::minutes(opts_.at("interval").as<int>()));
  checker_.async_wait(std::bind(&Bot::check_proxies, this));
}

void Bot::check_proxies() {
  Proxy::Vec proxy_vec;

  db_.get_proxies(proxy_vec);

  std::cout << "checking proxies. total: " << proxy_vec.size() << std::endl;

  Report::Ptr report = std::make_shared<Report>(proxy_vec.size());

  for (auto const& proxy : proxy_vec) {

    auto checker = [&, proxy, report]() {
      Pinger::Ptr pinger;

      std::string host;
      std::string err;

      Tools::extract_host_from_url(proxy->get_url(), host);

      if (host.empty()) {
        std::cerr << "no host in proxy url: " << proxy->get_id() << std::endl;
        return;
      }

      bool ping_result = pinger->ping(host, 1, err);
      proxy->set_last_check();

      if (ping_result) {
        report->active_.push_back(proxy);

        if (proxy->get_country_code().empty())
          proxy->set_country();

        Score score;

        if (proxy->get_fail_count() > 0) {
          proxy->dec_fail_count();
          score = Score::Up;

        } else
          score = Score::Same;

        io_.post(std::bind(&Bot::update_message, this, proxy, score));

      } else {
        report->failed_.push_back(proxy);

        if (proxy->enc_fail_count() == 5)
          report->removed_.push_back(proxy);
      }

      if (report->enc_ready() == report->total_)
        io_.post(std::bind(&Bot::process_report, this, report));

    };
    io_.post(checker);
  }

  start_checker();
}

void Bot::try_auth(TgBot::Message::Ptr message) {
  try {
    User::Vec users;

    db_.get_users(users, message);

    if (users.size() == 1) {
      User::Ptr user = users[0];

      user->set_username(message->from->username);
      db_.add_session(message, user);

      tg_bot_.getApi().sendMessage(message->chat->id, "Success. You will receiver proxy reports now");

      std::cout << "authenticated: " << user->get_username() << std::endl;

    } else {
      tg_bot_.getApi().sendMessage(message->chat->id, "Error. Please send a valid API key.");

      std::cerr << "bad auth:" << message->text << std::endl;
    }

  } catch (std::exception& e) {
    std::cerr << "error in auth: " << e.what() << " (" << message->text << ")" << std::endl;
  }
}

void Bot::update_message(Proxy::Ptr proxy, Score score) {
  try {
    std::lock_guard<std::mutex> locker(update_mutex_);

    TgBot::Chat::Ptr chat = tg_bot_.getApi().getChat(proxy->get_chat_id());

    std::ostringstream message;
    std::string score_emoji;

    switch (score) {
    case Score::Down:
      score_emoji = Emoji.at("Chart With Downwards Trend");
      break;

    case Score::Same:
      score_emoji = Emoji.at("Bar Chart");
      break;

    case Score::Up:
      score_emoji = Emoji.at("Chart With Upwards Trend");
      break;

    default:
      score_emoji = Emoji.at("Bar Chart");
      break;
    }

    std::string country_code = proxy->get_country_code();
    std::string country_name = proxy->get_country_name();

    message << proxy->get_message() << "\n\n"
            << Emoji.at("Hourglass") << " Checked: " << proxy->get_last_check() << " UTC\n"
            << (Flags.count(country_code) ? Flags.at(country_code) : Flags.at("White Flag"))
            << " Country: " << (Flags.count(country_code) ? country_name : "Unknown") << "\n"
            << score_emoji << " Fail Score: " << proxy->get_fail_count() << "/5\n\n"
            << Emoji.at("Copyright Sign") << " Stay alive @" << chat->username;

    tg_bot_.getApi().editMessageText(message.str(), proxy->get_chat_id(), proxy->get_message_id());

    db_.update_proxy(proxy);

    std::cout << "[" << std::this_thread::get_id() << "] updated proxy id: " << proxy->get_id() << " (" << proxy->get_url() << ")" << std::endl;

  } catch (std::exception& e) {
    std::cerr << "[" << std::this_thread::get_id() << "] update error for proxy id: " << proxy->get_id() << " (" << e.what() << ")" << std::endl;
  }
}

} // namespace Tnnl
