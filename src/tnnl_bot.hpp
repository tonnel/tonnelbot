#ifndef TNNL_BOT_HPP
#define TNNL_BOT_HPP

#include <map>
#include <mutex>

#include "boost/asio.hpp"
#include "boost/program_options.hpp"

#include "tgbot/tgbot.h"
#include "tnnl_db.hpp"
#include "tnnl_pinger.hpp"
#include "tnnl_proxy.hpp"
#include "tnnl_helper.hpp"

namespace b_po = boost::program_options;
namespace b_io = boost::asio;

namespace Tnnl {

class Report {
 public:
  using Ptr = std::shared_ptr<Report>;

  Report(int total): total_(total) {}

  const int total_;
  int ready_ = 0;

  Proxy::Vec active_;
  Proxy::Vec failed_;
  Proxy::Vec removed_;

  std::string admin_;

  int enc_ready() {return ++ready_;}

  friend std::ostream& operator<< (std::ostream& stream, const Report& R) {
    stream << "Hey, " << R.admin_ << "!\n"
           << "I have checked " << R.total_ << " proxies.\n"
           << Emoji.at("Checkmark") << " Active: " << R.active_.size() << " units,\n"
           << Emoji.at("White Question Mark") << " Failed: " << R.failed_.size() << " units,\n"
           << Emoji.at("White Exclamation Mark") << " Removed: " << R.removed_.size() << " units.";

    return stream;
  }
};

class ChannelPost {
 public:
  using Ptr = std::shared_ptr<ChannelPost>;

  ChannelPost() {}

  TgBot::Message::Ptr channel_message_;
  std::string proxy_url_;

  std::string& text() {return channel_message_->text;}
  bool has_url() {return !proxy_url_.empty();}
};

class Bot {
 public:
  Bot(b_po::variables_map& opts);
  ~Bot() {}

  void start_service();
  void start_polling();

  enum class Score {
    Down,
    Same,
    Up
  };

 private:
  TgBot::Bot tg_bot_;

  b_io::io_service io_;
  b_io::signal_set signals_;
  b_io::deadline_timer checker_;
  b_po::variables_map& opts_;

  std::mutex update_mutex_;

  DB db_;

  void setup_signals();
  void setup_handlers();

  void try_auth(TgBot::Message::Ptr);
  void try_add_proxy(ChannelPost::Ptr);

  bool ping_proxy(const std::string&);

  void process_update(TgBot::Update::Ptr);
  void process_report(Report::Ptr);

  void update_message(Proxy::Ptr, Score = Score::Same);
  void delete_message(Proxy::Ptr);
  void delete_message(ChannelPost::Ptr);

  void start_checker();
  void check_proxies();
};

} // namespace Tnnl

#endif // BOT_HPP
