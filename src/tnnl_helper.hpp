#ifndef TNNL_HELPER_HPP
#define TNNL_HELPER_HPP

#include <map>

constexpr unsigned int str_hash(const char* str, int h = 0) {
  return !str[h] ? 5381 : (str_hash(str, h + 1) * 33) ^ str[h];
}

constexpr auto operator  "" _(char const* p, size_t) {
  return str_hash(p);
}

namespace Tnnl {

extern const std::map<std::string, std::string> Emoji;
extern const std::map<std::string, std::string> Flags;

namespace Tools {
void extract_url_from_post(const std::string& msg, std::string& url);
void extract_host_from_url(const std::string& url, std::string& host);

bool check_proxy_url(const std::string& message);
}

} // namespace Tnnl

#endif // TNNL_HELPER_HPP
