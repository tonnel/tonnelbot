#ifndef TNNL_USER_HPP
#define TNNL_USER_HPP

#include <cstdint>
#include <string>
#include <iostream>
#include <memory>
#include <vector>

namespace Tnnl {

class User {
 public:
  using Ptr = std::shared_ptr<User>;
  using Vec = std::vector<User::Ptr>;

  User();
  ~User() {}

  void set_id(int32_t id) {id_ = id;}
  void set_username(const std::string& name) {username_ = name;}
  void set_key(const std::string& key) {key_ = key;}
  void set_super_user(bool super_user) {super_user_ = super_user;}

  int32_t get_id() const {return id_;}
  std::string& get_username() {return username_;}

 private:
  int32_t id_;
  std::string username_;
  std::string key_;
  bool super_user_;
};

} // namespace Tnnl

#endif // TNNL_USER_HPP
