#include "tnnl_pinger.hpp"

namespace Tnnl {

Pinger::Pinger() {

}

bool Pinger::ping(const std::string& address, int max_attempts, std::string& output) {
  std::string command = "ping -c " + num2str(max_attempts) + " " + address + " 2>&1";

  int code = exec_cmd(command, output);

  return (code == 0);
}

int Pinger::exec_cmd(const std::string& command, std::string& output, const std::string& mode) {
  std::stringstream sout;

  FILE* in;
  char buff[512];

  if (!(in = popen(command.c_str(), "r")))
    return 1;

  while (fgets(buff, sizeof(buff), in) != NULL)
    sout << buff;

  int exit_code = pclose(in);

  output = sout.str();

  return exit_code;
}

} // namespace Tnnl
