#ifndef TNNL_CURL_HPP
#define TNNL_CURL_HPP

#include <cstddef>
#include <string>
#include <functional>
#include <memory>
#include <iostream>

namespace Tnnl {

class Curl {
 public:
  using Ptr = std::shared_ptr<Curl>;
  using Handler = std::function<void(const std::string&)>;

  Curl();
  ~Curl() {}

  void http_get(const std::string&, const Handler&);

 private:

  std::string http_response_;
  static size_t curl_write(char* data, size_t size, size_t nmemb, void* stream);
  size_t curl_write_impl(char* data, size_t size, size_t nmemb);
};

} // namespace Tnnl

#endif // TNNL_CURL_HPP
