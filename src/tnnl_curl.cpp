#include "tnnl_curl.hpp"

#include <iostream>

#include "curl/curl.h"

namespace Tnnl {

Curl::Curl() {

}

void Curl::http_get(const std::string& url, const Handler& handler) {
  CURLcode res;
  CURL* curl = curl_easy_init();

  if (curl) {
    http_response_.clear();
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, this);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &Curl::curl_write);
    res = curl_easy_perform(curl);

    if (res != CURLE_OK)
      std::cerr << "http get error: " << curl_easy_strerror(res) << " (" << url.c_str() << ")" << std::endl;

    else
      handler(http_response_);

    curl_easy_cleanup(curl);
  }
}

size_t Curl::curl_write(char* data, size_t size, size_t nmemb, void* stream) {
  return static_cast<Curl*>(stream)->curl_write_impl(data, size, nmemb);
}

size_t Curl::curl_write_impl(char* data, size_t size, size_t nmemb) {
  http_response_.append(data, size * nmemb);
  return size * nmemb;
}

} // namespace Tnnl
