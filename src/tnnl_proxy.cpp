#include "tnnl_proxy.hpp"

#include "date.h"
#include "json.hpp"

#include "tnnl_curl.hpp"
#include "tnnl_helper.hpp"

namespace Tnnl {

Proxy::Proxy() {}

void Proxy::set_last_check() {
  using namespace date;
  using namespace std::chrono;

  auto tp = floor<seconds>(system_clock::now());

  std::ostringstream now;

  now << tp;

  set_last_check(now.str());
}

void Proxy::set_country() {
  std::string ip_api_url = "http://ip-api.com/json/";
  std::string host;

  Tools::extract_host_from_url(url_, host);

  if (host.empty()) {
    std::cerr << "error getting country for proxy id: " << id_ << ", url: " << url_ << std::endl;
    return;

  } else if (host == "tonnel.me") {
    country_code_ = "Globe With Meridians";
    country_name_ = "Multi Region";
    return;
  }

  Curl::Ptr curl = std::make_shared<Curl>();
  Curl::Handler handler = [&, curl](const std::string & response) {
    try {
      nlohmann::json json = nlohmann::json::parse(response);

      std::string status = json["status"].get<std::string>();

      if (status == "fail") {
        std::cerr << "error getting country for proxy id: " << id_
                  << ", query: " << json["query"].get<std::string>()
                  << ", error: " << json["message"].get<std::string>()
                  << std::endl;

      } else {
        country_code_ = json["countryCode"];
        country_name_ = json["country"];
      }

    } catch (std::exception& e) {
      std::cerr << "error processing http response: " << e.what() << ", (" << response << ")" << std::endl;
    }
  };

  ip_api_url.append(host);

  curl->http_get(ip_api_url, handler);
}

} // namespace Tnnl
