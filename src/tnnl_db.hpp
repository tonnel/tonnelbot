#ifndef TNNL_DB_HPP
#define TNNL_DB_HPP

#include <mutex>

#include "boost/asio.hpp"
#include "sqlite3.h"

#include "tgbot/tgbot.h"
#include "tnnl_user.hpp"
#include "tnnl_proxy.hpp"
#include "tnnl_session.hpp"

namespace Tnnl {

class ChannelPost;

class DB {
 public:
  DB(const std::string&);
  ~DB();

  Proxy::Ptr add_proxy(TgBot::Message::Ptr, const std::string&);

  void add_session(TgBot::Message::Ptr, User::Ptr);

  void get_proxies(Proxy::Vec&);
  void get_users(User::Vec&, TgBot::Message::Ptr);
  void get_sessions(Session::Vec&);

  void update_proxy(Proxy::Ptr);
  void delete_proxy(Proxy::Ptr);

  void update_user(User::Ptr);

 private:
  sqlite3* conn_;
  std::mutex mutex_;

  const std::string db_file_;

  void create_tables();
  static int dummy_cb(void*, int, char**, char**);
};

} // namespace Tnnl

#endif // TNNL_DB_HPP
