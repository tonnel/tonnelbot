#ifndef TNNL_SESSION_HPP
#define TNNL_SESSION_HPP

#include <vector>
#include <memory>
#include <cinttypes>

namespace Tnnl {

class Session {
 public:
  using Ptr = std::shared_ptr<Session>;
  using Vec = std::vector<Session::Ptr>;

  Session();

  void set_id(int32_t id) {id_ = id;}
  void set_user_id(int32_t user_id) {user_id_ = user_id;}
  void set_chat_id(int64_t chat_id) {chat_id_ = chat_id;}

  int32_t get_id() const {return id_;}
  int32_t get_user_id() const {return user_id_;}
  int32_t get_chat_id() const {return chat_id_;}

 private:
  int32_t id_;
  int32_t user_id_;
  int64_t chat_id_;
};

} // namespace Tnnl

#endif // TNNL_SESSION_HPP
