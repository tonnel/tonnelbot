#ifndef TNNL_PINGER_HPP
#define TNNL_PINGER_HPP

#include <cstdio>
#include <iostream>
#include <sstream>
#include <string>
#include <memory>

namespace Tnnl {

class Pinger {
 public:
  using Ptr = std::shared_ptr<Pinger>;

  Pinger();
  ~Pinger() {}

  bool ping(const std::string& address, int max_attempts, std::string& output);
 private:

  template <typename TP>
  TP str2num(std::string const& value) {
    std::stringstream sin;
    sin << value;
    TP output;
    sin >> output;
    return output;
  }

  template <typename TP>
  std::string num2str(TP const& value) {
    std::stringstream sin;
    sin << value;
    return sin.str();
  }

  int exec_cmd(const std::string& command, std::string& output, const std::string& mode = "r");

};

} // namespace Tnnl

#endif // TNNL_PING_HPP
