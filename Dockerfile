FROM fedora:28 AS build
COPY . /tonnel
RUN dnf install -y git cmake make gcc-c++ boost-devel curl-devel openssl-devel sqlite-devel && mkdir build && cd build && cmake ../tonnel && make
FROM fedora:28
RUN dnf upgrade -y libcurl openssl && dnf install -y iputils sqlite boost-thread boost-program-options boost-system
COPY --from=build /build/tonnelbot /usr/bin/tonnelbot
CMD /usr/bin/tonnelbot -k ${BOT_KEY} -d ${DB} -t ${THREADS}
