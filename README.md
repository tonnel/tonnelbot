# Tonnel Telegram Bot

Simple Telegram Bot for monitoring MT-Proto proxies in channel

![](img.png)

## Building

Currently tested on Fedora 28 only. Install prerequisites:

```bash
dnf install -y cmake make gcc-c++ boost-devel curl-devel openssl-devel sqlite-devel
```

Clone and build:

```bash
git clone git@gitlab.com:tonnel/tonnelbot.git
cd tonnelbot
git submodule init && git submodule update
cmake  .
make -j$(nproc --ignore=1)
sudo make install
```

## Running

```bash
tonnelbot -k <bot-key> -d <path-to-sqlite>
```

## Docker

```bash
docker run -d -v /data:/data \
       registry.gitlab.com/tonnel/tonnelbot \
       tonnelbot -k <bot-key> -d /data/data.db
```

## Options

```bash
  -d [ --db ] arg (=tonnel.db) Path to SQLite database.
  -k [ --key ] arg             Bot API key.
  -i [ --interval ] arg (=5)   Proxy check interval.
  -f [ --fail ] arg (=5)       Number of failed checks before proxy removal.
  -t [ --threads ] arg (=2)    Number of pinger threads.
  -h [ --help ]                Print this help message and exit.
```

## Third-party

- C++ library for Telegram bot API [https://github.com/reo7sp/tgbot-cpp](https://github.com/reo7sp/tgbot-cpp) ([MIT](https://github.com/reo7sp/tgbot-cpp/blob/master/LICENSE))
- JSON for Modern C++ [https://github.com/nlohmann/json](https://github.com/nlohmann/json) ([MIT](https://github.com/nlohmann/json/blob/develop/LICENSE.MIT))
- A date and time library based on the C++11/14/17 <chrono> header [https://github.com/HowardHinnant/date](https://github.com/HowardHinnant/date) ([MIT](https://raw.githubusercontent.com/HowardHinnant/date/master/LICENSE.txt))
