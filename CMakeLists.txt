cmake_minimum_required(VERSION 2.8)

project(tonnelbot)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release)
endif()

#set(CMAKE_C_COMPILER gcc)
#set(CMAKE_CXX_COMPILER g++)
#set(CMAKE_C_COMPILER clang)
#set(CMAKE_CXX_COMPILER clang++)

set(CMAKE_INSTALL_PREFIX /usr)

include(CheckCXXCompilerFlag)

CHECK_CXX_COMPILER_FLAG("-std=c++14" COMPILER_SUPPORTS_CXX14)

if(COMPILER_SUPPORTS_CXX14)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
    message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has C++14 support!")
else()
        message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++14 support. Please use a different C++ compiler.")
endif()

include_directories(./src ${CMAKE_SOURCE_DIR}/tgbot-cpp/include ${CMAKE_SOURCE_DIR}/date/include/date ${CMAKE_SOURCE_DIR}/json/single_include/nlohmann)

set(TGBOT_SRC
        tgbot-cpp/src/Api.cpp
        tgbot-cpp/src/TgTypeParser.cpp
        tgbot-cpp/src/TgException.cpp
        tgbot-cpp/src/EventHandler.cpp
        tgbot-cpp/src/net/Url.cpp
        tgbot-cpp/src/net/HttpClient.cpp
        tgbot-cpp/src/net/HttpParser.cpp
        tgbot-cpp/src/net/TgLongPoll.cpp
        tgbot-cpp/src/tools/StringTools.cpp
        tgbot-cpp/src/tools/FileTools.cpp
        tgbot-cpp/src/types/InlineQueryResult.cpp
        tgbot-cpp/src/types/InputFile.cpp
)

aux_source_directory(./src SRC)

add_library(TgBot STATIC ${TGBOT_SRC})

add_executable(${PROJECT_NAME} ${SRC})
target_link_libraries(${PROJECT_NAME} TgBot pthread ssl crypto boost_system boost_thread boost_program_options sqlite3 curl)

install (TARGETS ${PROJECT_NAME} RUNTIME DESTINATION bin)
